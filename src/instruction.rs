#![macro_use]

// region: IMPORTS

use crate::Run;
use snafu;
use std::ffi;
use std::fmt;
use std::ops;
use std::path;
use std::process;

// endregion: IMPORTS

// region: ERRORS

#[derive(Debug, snafu::Snafu)]
pub enum InstructionError {}

impl From<InstructionError> for crate::Error {
    fn from(instruction_error: InstructionError) -> Self {
        Box::new(instruction_error)
    }
}

// endregion: ERRORS

// region: INSTRUCTION

// TODO:
// 1. supply of pipe to stdin
// 2.

#[derive(Debug)]
#[cfg_attr(feature = "serde_support", derive(Serialize, Deserialize))]
pub struct Instruction {
    inner_command: std::process::Command,
}

pub type Command = Instruction;
pub type Directive = Instruction;

impl ops::Deref for Instruction {
    type Target = std::process::Command;

    fn deref(&self) -> &Self::Target {
        &self.inner_command
    }
}

impl ops::DerefMut for Instruction {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.inner_command
    }
}

impl Instruction {
    pub fn new<S: AsRef<ffi::OsStr>>(program: S) -> Instruction {
        Instruction {
            inner_command: std::process::Command::new(program),
        }
    }

    pub fn arg<S: AsRef<ffi::OsStr>>(&mut self, argument: S) -> &mut Instruction {
        self.inner_command.arg(argument);
        self
    }

    pub fn args<I, S>(&mut self, arguments: I) -> &mut Instruction
    where
        I: IntoIterator<Item = S>,
        S: AsRef<ffi::OsStr>,
    {
        self.inner_command.args(arguments);
        self
    }

    pub fn current_dir<P: AsRef<path::Path>>(&mut self, directory: P) -> &mut Instruction {
        self.inner_command.current_dir(directory);
        self
    }

    pub fn env<K, V>(&mut self, key: K, value: V) -> &mut Instruction
    where
        K: AsRef<ffi::OsStr>,
        V: AsRef<ffi::OsStr>,
    {
        self.inner_command.env(key, value);
        self
    }

    pub fn env_clear(&mut self) -> &mut Instruction {
        self.inner_command.env_clear();
        self
    }

    pub fn env_remove<K: AsRef<ffi::OsStr>>(&mut self, key: K) -> &mut Instruction {
        self.inner_command.env_remove(key);
        self
    }

    pub fn envs<I, K, V>(&mut self, variables: I) -> &mut Instruction
    where
        I: IntoIterator<Item = (K, V)>,
        K: AsRef<ffi::OsStr>,
        V: AsRef<ffi::OsStr>,
    {
        self.inner_command.envs(variables);
        self
    }

    pub fn stderr<T: Into<process::Stdio>>(&mut self, configuration: T) -> &mut Instruction {
        self.inner_command.stderr(configuration);
        self
    }

    pub fn stdin<T: Into<process::Stdio>>(&mut self, configuration: T) -> &mut Instruction {
        self.inner_command.stdin(configuration);
        self
    }

    pub fn stdout<T: Into<process::Stdio>>(&mut self, configuration: T) -> &mut Instruction {
        self.inner_command.stdout(configuration);
        self
    }
}

// endregion: INSTRUCTION

// region: LOGGING INFO

/// The logging data for an instruction. Contains the string form of the
/// instruction's program and the string form of its arguments
#[derive(fmt::Debug, Clone)]
#[cfg_attr(feature = "serde_support", derive(Serialize, Deserialize))]
struct LoggingData {
    program: String,
    arguments: Vec<String>,
}

/// Represents one token within the format specification of an instruction. The
/// format specification may have the instruction program, its arguments, and
/// arbitrary strings. Use the `new` and `append` methods to build up the format
#[derive(fmt::Debug, Clone)]
#[cfg_attr(feature = "serde_support", derive(Serialize, Deserialize))]
pub enum LoggingFormatToken {
    Program,
    Args,
    Output,
    ExitStatus,
    ArbitraryString(String),
}

pub type StringIteratorTransformer = Box<dyn FnMut(Box<dyn Iterator<Item = String>>) -> Box<dyn Iterator<Item = String>>>; 

/// The logging format for an instruction, in the format of an ordered list.
/// Each item in the list is a [LoggingFormatToken]
#[cfg_attr(feature = "serde_support", derive(Serialize, Deserialize))]
pub struct LoggingFormat {
    logging_format: Vec<LoggingFormatToken>,
    args_delimiter: String,
    output_delimiter: String,
    args_fn: Option<StringIteratorTransformer>,
    stdout_fn: Option<StringIteratorTransformer>,
    stderr_fn: Option<StringIteratorTransformer>,
    output_fn: Option<StringIteratorTransformer>,
}

pub type LoggingFormatBuilder = LoggingFormat;

impl ops::Deref for LoggingFormat {
    type Target = Vec<LoggingFormatToken>;

    fn deref(&self) -> &Self::Target {
        return &self.logging_format;
    }
}

impl ops::DerefMut for LoggingFormat {
    fn deref_mut(&mut self) -> &mut Self::Target {
        return &mut self.logging_format;
    }
}

impl LoggingFormat {
    /// Create a new instruction logging format with an empty list.
    pub fn new() -> Self {
        LoggingFormat {
            logging_format: Vec::new(),
            args_delimiter: String::default(),
            output_delimiter: String::default(),
            args_fn: None,
            stdout_fn: None,
            stderr_fn: None,
            output_fn: None,
        }
    }

    /// Append the instruction's program name to the end of the format
    /// specification
    pub fn append_program(mut self) -> Self {
        self.push(LoggingFormatToken::Program);
        return self;
    }

    /// Append the instruction's arguments to the end of the format
    /// specification
    pub fn append_args(mut self) -> Self {
        self.push(LoggingFormatToken::Args);
        return self;
    }

    /// Set the delimiter string that will be inserted between arguments
    pub fn args_delimiter<S: Into<String>>(mut self, delimiter_string: S) -> Self {
        self.args_delimiter = delimiter_string.into();
        return self;
    }

    /// Append the instruction's output to the end of the format specification
    pub fn append_output(mut self) -> Self {
        self.push(LoggingFormatToken::Output);
        return self;
    }

    /// Append the instruction's exit status to the end of the format
    /// specification
    pub fn append_exit_status(mut self) -> Self {
        self.push(LoggingFormatToken::ExitStatus);
        return self;
    }

    /// Append an arbitrary string to the end of the format specification
    pub fn append_string<S: Into<String>>(mut self, given_string: S) -> Self {
        self.push(LoggingFormatToken::ArbitraryString(given_string.into()));
        return self;
    }

    pub fn output_delimiter<S: Into<String>>(mut self, given_string: S) -> Self {
        self.output_delimiter = given_string.into();
        return self;
    }

    /// Provide a function or closure to transform an iterator over the
    /// arguments into another iterator over arguments, in order to transform
    /// the arguments. This function/closure could contain a chain of `map`
    /// and `filter` methods, for example.
    pub fn args_transform(
        mut self,
        transformer: StringIteratorTransformer,
    ) -> Self {
        self.args_fn = Some(transformer);
        return self;
    }

    /// Provide a function or closure to transform an iterator over the
    /// stdout lines into another iterator over stdout lines, in order to
    /// transform the stdout lines. This function/closure could contain a
    /// chain of `map` and `filter` methods, for example.
    pub fn stdout_transform(
        mut self,
        transformer: StringIteratorTransformer,
    ) -> Self {
        self.stdout_fn = Some(transformer);
        return self;
    }

    /// Provide a function or closure to transform an iterator over the
    /// stderr lines into another iterator over stderr lines, in order to
    /// transform the stderr lines. This function/closure could contain a
    /// chain of `map` and `filter` methods, for example.
    pub fn stderr_transform(
        mut self,
        transformer: StringIteratorTransformer,
    ) -> Self {
        self.stderr_fn = Some(transformer);
        return self;
    }

    /// Provide a function or closure to transform an iterator over the
    /// output lines into another iterator over output lines, in order to
    /// transform the output lines. This function/closure could contain a
    /// chain of `map` and `filter` methods, for example.
    pub fn output_transform(
        mut self,
        transformer: StringIteratorTransformer,
    ) -> Self {
        self.output_fn = Some(transformer);
        return self;
    }
}

impl Default for LoggingFormat {
    fn default() -> Self {
        LoggingFormat::new()
            .append_program()
            .append_string(" ")
            .append_args()
            .append_string(" \n")
            .append_output()
    }
}

// endregion: LOGGING INFO

// TESTS

// #[cfg(test)]
// mod tests {

//     // IMPORTS

//     #[cfg(feature = "async")]
//     use tokio::runtime::Runtime;

//     #[cfg(feature = "logging")]
//     use crate::tests::setup_logging;

//     // TESTS

//     #[test]
//     fn echo() {
//         #[cfg(feature = "logging")]
//         setup_logging(log::LevelFilter::Debug);

//         #[cfg(feature = "async")]
//         {
//             let runtime = Runtime::new().unwrap();
//             let output: String = String::from_utf8(
//                 runtime
//                     .block_on(async {
//                         super::Command::new("echo")
//                             .arg("Hello")
//                             .arg("World")
//                             .output()
//                             .await
//                     })
//                     .expect("Unable to run...")
//                     .stdout,
//             )
//             .expect("Unable to convert from utf-8 to String");
//             assert_eq!(output, String::from("Hello World\n"));
//         };

//         #[cfg(not(feature = "async"))]
//         {
//             let output: String = String::from_utf8(
//                 super::Command::new("echo")
//                     .arg("Hello")
//                     .arg("World")
//                     .output()
//                     .expect("Unable to run...")
//                     .stdout,
//             )
//             .expect("Unable to convert from utf-8 to String");
//             assert_eq!(output, String::from("Hello World\n"));
//         };
//     }
// }
